import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// 1http
import { HttpClientModule } from '@angular/common/http';
//2http
import { DatoService } from './dato.service';
import { FrausuarioComponent } from './frausuario/frausuario.component';
import { MiscuentasComponent } from './miscuentas/miscuentas.component';
import { InicioComponent } from './inicio/inicio.component';

//3 routes
import { Routes, RouterModule } from '@angular/router';
import { OperacionesComponent } from './operaciones/operaciones.component';
import { MasComponent } from './mas/mas.component';
import { UbicanosComponent } from './ubicanos/ubicanos.component';
import { NavComponent } from './nav/nav.component';
import { HeaderComponent } from './header/header.component';

const routes: Routes = [
{ path: '', component: InicioComponent },
   { path: 'miscuentas', component: MiscuentasComponent },
   { path: 'operaciones', component: OperacionesComponent },
   { path: 'ubicanos', component: UbicanosComponent },
   { path: 'mas', component: MasComponent },
 ];

@NgModule({
  declarations: [
    AppComponent,
    FrausuarioComponent,
    MiscuentasComponent,
    InicioComponent,
    OperacionesComponent,
    MasComponent,
    UbicanosComponent,
    NavComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule, 
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
	 HttpClientModule,
   RouterModule.forRoot(routes)
  ],
  providers: [DatoService],//3http
  bootstrap: [AppComponent]
})
export class AppModule { }
