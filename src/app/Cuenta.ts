export class Cuenta {

    public saldo: number;
    public tipo: number;
    public num_tarjeta: string;

	constructor(saldo?: number, tipo?: number, num_tarjeta?: string){
		this.saldo= saldo,
		this.tipo= tipo,
		this.num_tarjeta= num_tarjeta
	}
}
