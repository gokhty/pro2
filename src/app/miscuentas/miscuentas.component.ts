import { Component, OnInit } from '@angular/core';
import { DatoService } from '../dato.service';
import { Cuenta } from '../Cuenta';

@Component({
  selector: 'app-miscuentas',
  templateUrl: './miscuentas.component.html',
  styleUrls: ['./miscuentas.component.sass']
})
export class MiscuentasComponent implements OnInit {
 //variable que viene del login - '654326'
	usus = new Cuenta();
 arreglo = [];
 mititulo = 'Mis Cuentas'
  constructor( private srv : DatoService) {
	
  }

  ngOnInit() {
  this.srv.obtenerCuentas('654326').subscribe(dato =>{
      this.arreglo = dato;
    });
  }
  
	
tipos = ['Cuenta Simple Soles', 'Cuenta Vip', 'Cuenta de Ahorro'];
}
