import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './Usuario';
import { Cuenta } from './Cuenta';

@Injectable({
  providedIn: 'root'
})
export class DatoService {
	//url = 'https://jsonplaceholder.typicode.com/posts';
  url = 'http://localhost:3000/api/pruebas';
  urlcuenta = 'http://localhost:3000/api/cuentasXtarjeta';
  constructor(private http: HttpClient ) { 
    
  }
  obtenerDatos(){
    return this.http.get<Usuario[]>(this.url);
  }
  
  obtenerCuentas(num:string){
    return this.http.post<Cuenta[]>(this.urlcuenta, {
		numero: num
	});
  }
  
  enviarDatos(user: Usuario){
  	return this.http.post<any>(this.url, user);
  }
}
//https://www.youtube.com/watch?v=jwA-9XXybdM&t=8s